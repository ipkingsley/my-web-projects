import React from 'react';
import '../style/Prediction.css';

export default class Prediction extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: (props.locked && props.active) || false,
            value: props.value || "",
            error: props.error || "",
            label: props.label || "Please enter the text you want to analyze. (Max 3000 words)"
        };
    }

    onInput = (event) => {
        const value = event.target.value;
        this.setState({value, error: ""});
    };

    handleKeyPress = (event) => {
        if (event.which === 13) {
            alert(this.state.value);
        }
    };

    render() {
        const {active, value, error, label} = this.state;
        const locked = this.props.lock || false;
        const fieldClassName = `field ${(locked ? active : active || value) &&
        "active"} ${locked && !active && "locked"}`;

        return (
            <div className={fieldClassName}>
                <textarea
                    id="prediction-input"
                    className="input-box"
                    value={value}
                    placeholder={label}
                    onChange={this.onInput}
                    onKeyPress={this.handleKeyPress.bind(this)}
                    onFocus={() => !locked && this.setState({active: true})}
                    onBlur={() => !locked && this.setState({active: false})}
                    maxLength={3000}
                />
            </div>
        );
    }
}