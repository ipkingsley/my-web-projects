import React from 'react';
import '../style/About.css';
import Profile from './Profile';

import pic1 from '../pics/kingsley.png';
import pic2 from '../pics/2.png';
import pic3 from '../pics/john.png';
import pic4 from '../pics/4.jpeg';
import pic5 from '../pics/5.jpeg';
import pic6 from '../pics/Irfan.jpeg';

const profiles = [
    ['Mustafa Irfan', 'F', pic6],
    ['Pranav Râm', 'E', pic5],
    ['Henok Tadesse', 'IBM Full-Stack Developer. #I am graduating', pic2],
    ['John Truong', 'Software Developer, Data and business analyst', pic3],
    ['Andrew Vu', 'D', pic4],
    ['Qian Wen', 'A', pic1]
];

const listProfiles = profiles.map((profile, i) => (
    <Profile key={profile.toString()} name={profile[0]} description={profile[1]} src={profile[2]} index={i}/>
));

export default () => (
    <div className='about background'>
        <h4>Contributors</h4>
        <div className='fit-center'>
            {listProfiles}
        </div>
        <br/><br/>
        <div className='technology'>
            <h4>Technologies</h4>
        </div>
    </div>
);