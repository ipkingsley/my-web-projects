import React from 'react';
import Navbar from "react-bootstrap/Navbar";
import {Link} from "react-router-dom";
import '../style/Header.css';

export default () => (
    <Navbar bg="dark" variant="dark" className="header">
        <Link to="/"><h4>PSD</h4></Link>
        <Link to="/">Home</Link>
        <Link to="/presidents">Presidents</Link>
        {/*<Link to="/feature">Feature</Link>*/}
        <Link to="/about">About</Link>
    </Navbar>);