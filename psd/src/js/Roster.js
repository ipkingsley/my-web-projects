import React from "react";
import {Switch, Route} from "react-router-dom";
import Presidents from './Presidents';
import About from './About';
import Home from './Home';
// import Feature from './Feature';
import Analysis from './Analysis';
import posed, {PoseGroup} from 'react-pose';
import '../style/Roster.css';

const RouteContainer = posed.div({
    enter: {opacity: 1, delay: 300, beforeChildren: true},
    exit: {opacity: 0}
});

export default () => (
    <Route
        render={({location}) => (
            <div className='roster'>
                <PoseGroup>
                    <RouteContainer key={location.key}>
                        <Switch location={location}>
                            <Route exact path="/" component={Home} key="home"/>
                            <Route path="/about" component={About} key="about"/>
                            <Route path="/presidents" component={Presidents} key="presidents"/>
                            {/*<Route path="/feature" component={Feature} key="feature"/>*/}
                            <Route path="/obama" component={() => (<Analysis name="Barack Obama"/>)} key="obama"/>
                            <Route path="/wbush" component={() => (<Analysis name="George W. Bush"/>)} key="wbush"/>
                            <Route path="/bclinton" component={() => (<Analysis name="Bill Clinton"/>)} key="bclinton"/>
                        </Switch>
                    </RouteContainer>
                </PoseGroup>
            </div>
        )}
    />
);