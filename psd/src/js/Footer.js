import React from 'react';
import Navbar from "react-bootstrap/Navbar";
import '../style/Footer.css';
import {Link} from "react-router-dom";

export default () => (
    <Navbar bg="dark" variant="dark" className='footer header'>
        Copyright © 2019&nbsp;
        <Link to="/">Presidential Speech Detectives</Link>
    </Navbar>
);