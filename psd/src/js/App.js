import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import '../style/App.css';
import Roster from "./Roster";
import {BrowserRouter as Router} from "react-router-dom";
import Header from "./Header";
import Footer from "./Footer";

export default () => (
    <Router>
        <Header/>
        <Roster/>
        <Footer/>
    </Router>
);

