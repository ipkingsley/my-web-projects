import React from 'react';
import Flippy, {FrontSide, BackSide} from 'react-flippy';
import RoundImg from './RoundImg';
import '../style/Profile.css';

export default class Profile extends React.Component {
    render() {
        return (
            <div className='profile'>
                <Flippy flipOnHover={true} flipDirection="horizontal"
                        showNavigation={false} flipOnClick={true}
                        usePrettyStyle={true} style={{width: '18vw', height: '18vw'}}>
                    <FrontSide className='profile-front'>
                        <RoundImg src={this.props.src} alt={this.props.name}/>
                        {this.props.name}
                    </FrontSide>
                    <BackSide style={{backgroundColor: '#bdc2e8'}}>
                        {this.props.description}
                    </BackSide>
                </Flippy>
            </div>
        );
    }
}