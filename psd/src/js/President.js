import React from 'react';
import {Link} from 'react-router-dom';
import '../style/President.css';

export default class President extends React.Component {
    render() {
        return (
            <Link to={this.props.href}>
                <div className="president" style={this.getStyle(this.props.party)}>
                    <img src={this.props.src} className="picture" alt={this.props.name}/>
                    <div className="info">
                        <h4>{this.props.name}</h4>
                        {this.props.party + " Party"}<br/>
                        {this.props.about}
                    </div>
                </div>
            </Link>
        );
    }

    getStyle = (party) => {
        return (party === "Democratic") ?
            {
                backgroundColor: `#8BC6EC`,
                backgroundImage: `linear-gradient(135deg, #8BC6EC 0%, #003FE9 100%)`
            } :
            {
                backgroundColor: `#FAD961`,
                backgroundImage: `linear-gradient(90deg, #F76B1C 40%, red 100%)`
            };
    }
}