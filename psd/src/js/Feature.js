import React from 'react';
import Prediction from './Prediction';
import '../style/Feature.css';

export default () => (
    <div className='feature background'><Prediction/></div>
);