import React from 'react';
import '../style/RoundImg.css';

export default class RoundImg extends React.Component {
    render() {
        return (
            <div className='round'>
                <img src={this.props.src} alt={this.props.name} id={this.props.index}/>
            </div>
        );
    }
}