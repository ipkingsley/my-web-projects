export const fetchUrl = (url, data) => {
    fetch(url, {
        mode: 'same-origin',
        method: 'POST',
        headers:
            {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        credentials: "same-origin",
        body: JSON.stringify(data)
    })
        .then((response) => response.text())
        .then((resp) => {
            return resp;
        }).catch(function (error) {
        console.log('Request failed', error)
    });
};