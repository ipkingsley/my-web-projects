import React from 'react';
import posed from 'react-pose';
import President from './President';
import '../style/Presidents.css';

import obama from '../pics/obama.jpg';
import bush from '../pics/wbush.jpg';
import clinton from '../pics/clinton.jpg';

export default () => (
    <Container className='presidents background'>
        {presidents}
    </Container>
);

const Container = posed.div({
    enter: {staggerChildren: 50}
});

const P = posed.div({
    enter: {x: 0, opacity: 1},
    exit: {x: 50, opacity: 0},
});

const presidentInfo = [
    ["Barack Obama", obama, "Democratic", "/obama",
        "Barack Hussein Obama II is an American attorney and politician who served as the 44th president of the United States from 2009 to 2017. "],
    ["George W. Bush", bush, "Republican", "/wbush",
        "George Walker Bush is an American politician and businessman who served as the 43rd president of the United States from 2001 to 2009."],
    ["Bill Clinton", clinton, "Democratic", "/bclinton",
        "William Jefferson Clinton is an American politician who served as the 42nd president of the United States from 1993 to 2001."]
];

const presidents = presidentInfo.map((president) => (
    <P className="president-container" key={president.toString()}><President name={president[0]} party={president[2]} href={president[3]} src={president[1]} about={president[4]}/></P>
));