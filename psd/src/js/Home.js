import React from 'react';
import posed from 'react-pose';
import '../style/Home.css';

export default () => (
    <Container className="home background">
        <P><h4 className="text-center">Motivation</h4></P>
        <P className="text-left">When running as a presidential candidate, it is crucial for you to get your political
            views across the people and show them exactly where you stand. However, due to each
            region being politically independent on specific policies, the ideal presidential candidate
            cannot support all areas satisfying all regions at once. <br/><br/>
        </P>
        <P className="text-left">The candidate must choose the areas he/she would like to support even though it might
            dissatisfy some people. Our team decided to analyze the trends in the political bias
            of presidential candidates and how they changed over time particularly during election
            times and campaigns. <br/><br/>
        </P>
        <P className="text-left">Natural language processing techniques will be used to analyze the trends in different
            areas such as healthcare, tax policy, education, and national security. NLP techniques
            coupled with LSTM neural networks will create sophisticated models to decipher the difference
            between conservative and liberal views.
        </P>
        </Container>
);

const Container = posed.div({
    enter: {staggerChildren: 50}
});

const P = posed.div({
    enter: {x: 0, opacity: 1},
    exit: {x: 50, opacity: 0},
});