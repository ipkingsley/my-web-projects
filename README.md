These projects are for WEB development using HTML, CSS, JavaScript, and more.

About Me: This is a personal website containging brief information about myself. The website is hosting on github. URL: www.kingsleywen.com

FakeBingWithGoogleSearch: This is a project I have done for fun. Basically, the website is protending to be Bing Search Website, but when you actually click on the Bing logo or search through the search bar, it will bring up search result from Google search engine.

Shopping List: This is a simple shopping list webapp built using REACT. It can allow users to create a list of items and calculate the sub-total. It also allows the users to export the list as a json file. Now, I am working on making the list persistent across refresh.
