import React from 'react';
import './Item.css';

const style = {
    display: 'inline'
}
class Item extends React.Component {
    render() {
        return (
            <div className="Item" style={style}>
                <button>
                    {this.props.name}, costs ${this.props.cost} per unit
                </button>
                <p>Count: {this.props.count}</p>
            </div>
        )
    }
}

export default Item;
