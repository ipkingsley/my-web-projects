import React, { Component } from 'react';

const RemoveItem = (props) => {
    return (
        <div>
            <input type='text' onChange={props.changed}/>
            <button onClick={props.removed}>Remove!</button>
        </div>
    );
}
export default RemoveItem;
