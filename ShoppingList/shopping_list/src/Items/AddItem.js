import React, { Component } from 'react';

const AddItem = (props) => {
    return (
        <div>
            <input type='text' onChange={props.newName}/>
            <input type='text' onChange={props.newPrice} />
            <button onClick={props.added}>Add!</button>
        </div>
    );
}
export default AddItem;
