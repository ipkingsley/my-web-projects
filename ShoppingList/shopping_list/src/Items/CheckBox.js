import React from 'react';

class CheckBox extends React.Component {
    render() {
        const style = {
            display: 'inline',
            padding: '5px'
        }
        return (
            <input type="checkbox" style={style} id={this.props.id} value={this.props.value} onChange={this.props.onChange} />
        )
    }
}

class Controls extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            optionsChecked: []
        }
    }

    changeEvent(event) {
        let checkedArray = this.state.optionsChecked;
        let selectedValue = event.target.value;

        if(event.target.checked === true) {
            checkedArray.push(selectedValue);
            this.setState({optionsChecked: checkedArray});
        }
        else {
            let valueIdx = checkedArray.indexOf(selectedValue);
            checkedArray.splice(valueIdx, 1);
            this.setState({optionsChecked: checkedArray});
        }
    }

}

export default CheckBox;
