import React, { Component } from 'react';

const style = {
    display: 'inline'
}

class DecreaseItem extends React.Component {
    state = {
        id: ' '
    }

    render() {
        return (<button onClick={this.props.decreased} style={style} className={this.props.id}>-</button>)
    }
}

export default DecreaseItem;
