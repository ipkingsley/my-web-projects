import React, { Component } from 'react';
import Item from './Items/Item';
import RemoveItem from './Items/RemoveItem';
import AddItem from './Items/AddItem';
import IncreaseItem from './Items/IncreaseItem';
import DecreaseItem from './Items/DecreaseItem';
import CheckBox from './Items/CheckBox';

class App extends Component {
    state = {
        items: [],
        itemCount: 0,
        total: 0,
        userText: ' ',
        newItemName: ' ',
        newItemPrice: ' ',
        optionsChecked: []
    }
    newTotalCost() {
        let newTotal = 0;
        for(const item of this.state.items) {
            newTotal += Number(item.cost)*Number(item.count);
        }
        this.setState({total: newTotal});
    }

    onChangeHandler = (event) => {
        this.setState({ userText: event.target.value });
    }

    addItemNameHandler = (event) => {
        this.setState({ newItemName: event.target.value });
    }

    addItemPriceHandler = (event) => {
        this.setState({ newItemPrice: event.target.value });
    }

    clickHandler = () => {
        const oldPrice =  this.state.total;
        const newPrice = oldPrice + 1;
        this.setState({total: newPrice});
    }

    deleteHandler = () => {
        const ItemIdx = this.state.items.findIndex(el => {
            return el.name === this.state.userText;
        });

        if(ItemIdx >= 0) {
            let newItems = [...this.state.items];
            newItems.splice(ItemIdx, 1);
            let newCount = this.state.itemCount - 1;
            this.setState({items: newItems});
            this.setState({itemCount: newCount});

            let newTotal = 0;
            for(const item of newItems) {
                newTotal += Number(item.cost)*Number(item.count);
            }
            this.setState({total: newTotal});
        }
        else {
            this.setState({userText: 'Not found!'});
        }
    }

    addHandler = () => {
        const ItemIdx = this.state.items.findIndex(el => {
            return (el.name === this.state.newItemName && el.cost == this.state.newItemPrice);
        });
        let newItems;
        if(ItemIdx >= 0) {
            newItems = [...this.state.items];
            newItems[ItemIdx].count += 1;
            this.setState({items: newItems});
        }
        else {
            const newIdx = this.state.itemCount + 1;
            newItems = [...this.state.items, { key: newIdx, name: this.state.newItemName, cost: this.state.newItemPrice, count: 1}];
            this.setState({items: newItems});
            this.setState({itemCount: newIdx});
        }

        let newTotal = 0;
        for(const item of newItems) {
            newTotal += Number(item.cost)*Number(item.count);
        }
        this.setState({total: newTotal});
    }

    increaseHandler = (e) => {
        const name = e.target.className;
        const ItemIdx = this.state.items.findIndex(el => {
            return (el.name === name);
        });
        let newItems = [...this.state.items];
        newItems[ItemIdx].count += 1;
        this.setState({items: newItems});
        let newTotal = 0;
        for(const item of newItems) {
            newTotal += Number(item.cost)*Number(item.count);
        }
        this.setState({total: newTotal});
    }

    decreaseHandler = (e) => {
        const name = e.target.className;
        const ItemIdx = this.state.items.findIndex(el => {
            return (el.name === name);
        });
        let newItems = [...this.state.items];
        newItems[ItemIdx].count -= 1;
        if(newItems[ItemIdx].count <= 0) {
            newItems.splice(ItemIdx, 1);
        }
        this.setState({items: newItems});
        let newTotal = 0;
        for(const item of newItems) {
            newTotal += Number(item.cost)*Number(item.count);
        }
        this.setState({total: newTotal});
    }

    changeEvent = (event) => {
        let checkedArray = this.state.optionsChecked;
        let selectedValue = event.target.value;

        if(event.target.checked === true) {
            checkedArray.push(selectedValue);
            this.setState({optionsChecked: checkedArray});
        }
        else {
            let valueIdx = checkedArray.indexOf(selectedValue);
            checkedArray.splice(valueIdx, 1);
            this.setState({optionsChecked: checkedArray});
        }
    }

    render() {
        const style = {
            textAlign: 'center',
            fontSize: '25px'
        }

        const divStyle = {
            textAlign: 'center'
        }

        return (
            <div style={divStyle}>
                {this.state.items.map((el,index) => {
                    return (<div>
                        <CheckBox value={el.name + ' x ' + el.count} id={'string_' + index} onChange={this.changeEvent} />
                        <Item key={index} name={el.name} cost={el.cost} count={el.count} clicked={this.clickHandler}/>
                    <DecreaseItem id={el.name} decreased={this.decreaseHandler} /><IncreaseItem id={el.name} increased={this.increaseHandler} />
                    </div>);
                })}
                <AddItem
                    newName={this.addItemNameHandler}
                    newPrice={this.addItemPriceHandler}
                    added={this.addHandler} />
                <RemoveItem
                    changed={this.onChangeHandler}
                    removed={this.deleteHandler} />
            <p style={style}>Total cost before tax: ${this.state.total}</p>
            <p>{JSON.stringify(this.state.optionsChecked, null, 2)}</p>
            </div>
        );
    }
}

export default App;
