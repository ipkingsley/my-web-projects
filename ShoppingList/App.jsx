import React, { Component } from 'react';

class App extends Component {
    state = {
        total: 0;
    }

    clickHandler = () => {
        const oldPrice =  this.state.total;
        const newPrice = oldPrice + 1;
        this.setState({total: newPrice});
    }

    render() {
        return (
            <div>
                <button onClick={this.clickHandler}>Clicked</button>
                <p>Total Price: {this.state.total}</p>
            </div>
        )
    }
}

export default App;
