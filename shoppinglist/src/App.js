import React, {Component} from 'react';
import Item from './Items/Item';
import AddItem from './Items/AddItem';
import IncreaseItem from './Items/IncreaseItem';
import DecreaseItem from './Items/DecreaseItem';
import CheckBox from './Items/CheckBox';
import './App.css';
import "font-awesome/css/font-awesome.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import {MDBBtn} from 'mdbreact';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            itemCount: 0,
            total: 0,
            userText: ' ',
            newItemName: ' ',
            newItemPrice: ' ',
            optionsChecked: []
        };
        this.reloadList = this.reloadList.bind(this);
    }

    componentDidMount() {
        window.addEventListener('load', this.reloadList());
    }

    computeTotal = (items) => {
        let newTotal = 0;
        for (const item of items) {
            newTotal += Number(item.unit_cost) * Number(item.count);
        }
        newTotal = newTotal.toFixed(2);
        this.setState({total: newTotal});
    };

    reloadList = () => {
        const cached = localStorage.getItem('items');
        if (cached) {
            const items = JSON.parse(cached);
            this.setState({items: items});
            this.computeTotal(items);
        }
    };

    exportJson = () => {
        let element = document.createElement("a");
        let file = new Blob(['{"Total Cost": "$' + this.state.total + '", "Items":', JSON.stringify(this.state.items, null, 2), '}'], {type: 'text/plain'});
        element.href = URL.createObjectURL(file);
        element.download = "shopping_list.json";
        element.click();
    };

    saveList = () => {
        localStorage.setItem('items', JSON.stringify(this.state.items));
    };

    newTotalCost() {
        let newTotal = 0;
        for (const item of this.state.items) {
            newTotal += Number(item.unit_cost) * Number(item.count);
        }
        this.setState({total: newTotal});
    };

    onChangeHandler = (event) => {
        this.setState({userText: event.target.value});
    };

    addItemNameHandler = (event) => {
        this.setState({newItemName: event.target.value});
    };

    addItemPriceHandler = (event) => {
        this.setState({newItemPrice: event.target.value});
    };

    clickHandler = () => {
        const oldPrice = this.state.total;
        const newPrice = oldPrice + 1;
        this.setState({total: newPrice});
    };

    addHandler = () => {
        const ItemIdx = this.state.items.findIndex(el => {
            return (el.name === this.state.newItemName && el.unit_cost === this.state.newItemPrice);
        });
        let newItems;
        if (ItemIdx >= 0) {
            newItems = [...this.state.items];
            newItems[ItemIdx].count += 1;
            this.setState({items: newItems});
        } else {
            const newIdx = this.state.itemCount + 1;
            newItems = [...this.state.items, {
                name: this.state.newItemName,
                unit_cost: this.state.newItemPrice,
                count: 1
            }];
            this.setState({items: newItems});
            this.setState({itemCount: newIdx});
        }

        this.computeTotal(newItems);
    };

    increaseHandler = (e) => {
        const name = e.target.className;
        const ItemIdx = this.state.items.findIndex(el => {
            return (el.name === name);
        });
        let newItems = [...this.state.items];
        newItems[ItemIdx].count += 1;
        this.setState({items: newItems});
        this.computeTotal(newItems);
    };

    decreaseHandler = (e) => {
        const name = e.target.className;
        const ItemIdx = this.state.items.findIndex(el => {
            return (el.name === name);
        });
        let newItems = [...this.state.items];
        newItems[ItemIdx].count -= 1;
        if (newItems[ItemIdx].count <= 0) {
            newItems.splice(ItemIdx, 1);
        }
        this.setState({items: newItems});
        this.computeTotal(newItems);
    };

    checkHandler = (event) => {
        let checkedArray = this.state.optionsChecked;
        let selectedValue = event.target.value;

        if (event.target.checked === true) {
            checkedArray.push(selectedValue);
            this.setState({optionsChecked: checkedArray});
        } else {
            let valueIdx = checkedArray.indexOf(selectedValue);
            checkedArray.splice(valueIdx, 1);
            this.setState({optionsChecked: checkedArray});
        }
    };

    render() {
        const style = {
            textAlign: 'center',
            fontSize: '25px'
        };

        const divStyle = {
            textAlign: 'center'
        };

        return (
            <div style={divStyle} className='whole center'>
                {this.state.items.map((el, index) => {
                    return (
                        <div className='list center'>
                            <CheckBox
                                value={'{"Item": "' + el.name + '","Count": "' + el.count + '","unit_cost": "$' + el.unit_cost + '","Cost": "$' + el.count * el.unit_cost + '"}'}
                                id={'string_' + index}
                                onChange={this.checkHandler}/>
                            <Item key={index} name={el.name} cost={el.unit_cost} count={el.count}
                                  clicked={this.clickHandler}
                            />
                            <DecreaseItem id={el.name} decreased={this.decreaseHandler}/>
                            <IncreaseItem id={el.name} increased={this.increaseHandler}/>
                        </div>
                    );
                })}
                <div className='btns'>
                    <AddItem
                        newName={this.addItemNameHandler}
                        newPrice={this.addItemPriceHandler}
                        added={this.addHandler}/>
                    <p style={style}>Total cost before tax: ${this.state.total}</p>
                    <MDBBtn onClick={this.exportJson}>Export List</MDBBtn>
                    <MDBBtn onClick={this.saveList}>Save List</MDBBtn>
                    <MDBBtn onClick={this.reloadList}>Reload List</MDBBtn>
                </div>
            </div>
        );
    }
}

export default App;
