import React from 'react';

const style = {
    display: 'inline',
    width: '30px',
    height: '30px'
}

class DecreaseItem extends React.Component {
    state = {
        id: ' '
    }

    render() {
        return (<button onClick={this.props.decreased} style={style} className={this.props.id}>-</button>)
    }
}

export default DecreaseItem;
