import React from 'react';
import {MDBInput} from 'mdbreact';
import {MDBBtn} from 'mdbreact';
import '../App.css';
import "font-awesome/css/font-awesome.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";

const RemoveItem = (props) => {
    return (
        <div className='form center'>
            <MDBInput label='Item Name' onChange={props.changed}/>
            <MDBBtn onClick={props.removed}>Remove Item</MDBBtn>
        </div>
    );
}
export default RemoveItem;
