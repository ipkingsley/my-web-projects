import React from 'react';
import {Input} from 'mdbreact';
import '../App.css';
import "font-awesome/css/font-awesome.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";

class CheckBox extends React.Component {
    render() {
        const style = {
            width: '30px',
            height: '30px',
            position: 'relative',
            display: 'inline',
            float: 'left',
            margin: '18px 20px 0 0',
            radius: '2px'
        }
        return (
            <Input type="checkbox" style={style} id={this.props.id} value={this.props.value} onChange={this.props.onChange} />
        )
    }
}

class Controls extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            optionsChecked: []
        }
    }

    changeEvent(event) {
        let checkedArray = this.state.optionsChecked;
        let selectedValue = event.target.value;

        if(event.target.checked === true) {
            checkedArray.push(selectedValue);
            this.setState({optionsChecked: checkedArray});
        }
        else {
            let valueIdx = checkedArray.indexOf(selectedValue);
            checkedArray.splice(valueIdx, 1);
            this.setState({optionsChecked: checkedArray});
        }
    }

}

export default CheckBox;
