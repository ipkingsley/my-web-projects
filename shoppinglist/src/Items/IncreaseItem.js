import React from 'react';

const style = {
    display: 'inline',
    width: '30px',
    height: '30px'
}

class IncreaseItem extends React.Component {
    state = {
        id: ' '
    }

    render() {
        return (<button onClick={this.props.increased} style={style} className={this.props.id}>+</button>)
    }
}

export default IncreaseItem;
