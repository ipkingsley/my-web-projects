from bs4 import BeautifulSoup
from urllib2 import urlopen
from urllib2 import HTTPError, URLError

result = []

try:
    html = urlopen("")
except HTTPError as e:
    print(e)
except URLError:
    print("Server down or incorrect domain")
else:
    html = BeautifulSoup(html.read(), "html5lib")
    tags = html.findAll("h2", {"class": "widget-title"})
    for tag in tags:
        print(tag.getText())
